<?php
include 'auth/authForm.php';
top();
?>

<!-- content -->
    <section id="content">
        <div class="bg-top">
            <div class="bg-top-2">
                <div class="bg">
                    <div class="bg-top-shadow">
                        <div class="main">
                            <div class="box p3">
                                <div class="padding">
                                    <div class="container_12">
                                        <div class="wrapper">
                                            <div class="grid_12">
                                                <div class="wrapper">

												<div class="indent-left p2">
													<h3 class="hello_user p1"><i style="color: red"><?php echo $_SESSION['email'] ?></i></h3>
                                                    <div class="editP"><a href="/profile/edit"><img src="/images/edit.png" alt="Редактировать профиль"></a></div>
                                                    <div class="exitP"><a href="/logout"><img src="/images/exit.png" alt="Выход из профиля"></a></div>
												</div>

                                                    <div class="main_users_menu">

                                                        <div class="sale">
                                                        <p>Ваша скидка:  <b class="price_sale"> 5%</b></p>
                                                        </div>
                                                        <hr>
                                                    </div>

                                                    <img style="float: right;width: 140px; height: 115px" src="/images/error/editProject.png">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php 
bottom();
footer();
?>