<?php
require_once 'auth/authForm.php';
top();
?>


<!-- content -->
<section id="content">
    <div class="bg-top">
        <div class="bg-top-2">
            <div class="bg">
                <div class="bg-top-shadow">
                    <div class="main">
                        <div class="box p3">
                            <div class="padding">
                                <div class="container_12">
                                    <div class="wrapper">
                                        <div class="grid_12">
                                            <div class="wrapper">

                                                <div class="indent-left p2">
                                                    <h3 class="hello_user p1"><i style="color: red"><?php echo $_SESSION['email'] ?></i></h3>
                                                    <div class="profiles"><a href="/profile"><img src="/images/profile.png" alt="Редактировать профиль"></a></div>
                                                    <div class="exitP"><a href="/logout"><img src="/images/exit.png" alt="Выход из профиля"></a></div>

                                                    <div class="main_users_menu">
                                                        <hr>
                                                    </div>

                                                    <h4>Изменить пароль:</h4>
                                                    <div class="container">
                                                                <input class="input-header" type="password" id="password" placeholder=" Новый пароль" />
                                                        <h4 style="margin-top: 10px">Список IP:</h4>
                                                                <input class="input-header" type="text" id="ip" value="<?=$_SESSION['ip']?>" placeholder=" IP адресс" />

                                                        <div style="margin-top: 17px">
                                                        <select id="protected">
                                                            <?=str_replace('"'.$_SESSION['protected'].'"','"'.$_SESSION['protected'].'" selected','<option value="0">Подтверждение входа Выкл.</option>
                                                        <option value="1">Подтверждение входа Вкл.</option>');?>
                                                        </select>
                                                        </div>

                                                            <input type="submit" class="glo" onclick="post_query('authForm','edit','password.ip.protected')" value="Сохранить">
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
bottom();
footer();
?>
