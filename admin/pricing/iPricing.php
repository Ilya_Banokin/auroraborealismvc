<?php
require_once 'admin/module/modPricing.php';

if (isset($_FILES['image']['name'])) {
    add_image ();
}

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}else {
    $action = "";
}


if ($action == "add")
{
    if(!empty($_POST)) {
        articles_new();
        header("Location: apricing");
    }
    include 'admin/forms/fpricing.php';
}
else if($action == "edit")
{
    if(!isset($_GET['id']))
    {
        exit('ID не найден');
    }elseif (!empty($_POST) && $_GET['id'] > 0) {
        articles_edit();
        header("Location: apricing");
    }
    require_once 'admin/forms/fpricing.php';
}
else if($action == "delete")
{
    $article = articles_delete();
    header("Location: apricing");
}
else {
    include 'admin/pricing/page/aPricing.php';
}
