<?php
require_once 'admin/module/aDesign.php';
function get_news ()
{
    $db = new Db();

    $news = $db->row('SELECT * FROM `news` ORDER BY `id` DESC');

    return $news;
}
function articles_get () {
    $db = new Db();

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $article = $db->row("SELECT * FROM `news` WHERE `id`= :id",$params);

    return $article;
}
function articles_new()
{
    $db = new Db();

    $params = [
        'date' => date("Y-m-d"),
        'text' => $_POST['text'],
    ];

    $db->query("INSERT INTO `news`(`date`,`text`) VALUES (:date,:text)",$params);
}
function articles_edit() {
    $db = new Db();

    $params = [
        'date' => date("Y-m-d"),
        'text' => $_POST['text'],
        'id' => (int)$_GET['id'],
    ];

    $db->query("UPDATE `news` SET `date` = :date,`text` = :text WHERE `id` = :id",$params);
}
function articles_delete(){
    $db = new Db();

    if ($_GET['id'] == 0) {
        exit('ID не найден');
    }

    $params = [
        'id' => $_GET['id'],
    ];

    $db->query("DELETE FROM `news` WHERE `id`= :id",$params);
}