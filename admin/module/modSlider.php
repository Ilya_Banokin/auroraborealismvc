<?php
require_once 'admin/module/aDesign.php';
function get_slider ()
{
    $db = new Db();

    $slider = $db->row('SELECT * FROM `slider`');

    return $slider;
}
function articles_get () {
    $db = new Db();

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $article = $db->row("SELECT * FROM `slider` WHERE `id`= :id",$params);

    return $article;
}
function articles_edit() {
    $db = new Db();

    $params = [
        'mini_image' => date("dmyi").'.jpg',
        'image' => date("dmyi").'.jpg',
        'id' => (int)$_GET['id'],
    ];

    $db->query("UPDATE `slider` SET `mini_image` = :mini_image,`image` = :image WHERE `id` = :id",$params);
}
function articles_delete(){
    $db = new Db();

    if ($_GET['id'] == 0) {
        exit('ID не найден');
    }

    $params = [
        'id' => $_GET['id'],
    ];

    $db->query("DELETE FROM `slider` WHERE `id`= :id",$params);
}
function add_image ()
{
    $upload_photo = $_FILES['image']['name']; // загружаемый из формы из поля upload_photo файл

    $path_to_90_directory = 'images/slide/slider_mini/';//папка, куда будет загружаться начальная картинка и ее сжатая копия

    if(preg_match('/[.](JPG)|(jpg)|(gif)|(GIF)|(png)|(PNG)$/',$_FILES['image']['name']))//проверка формата исходного изображения
    {

        $filename = $_FILES['image']['name'];
        $source = $_FILES['image']['tmp_name'];
        $target = $path_to_90_directory . $filename;
        move_uploaded_file($source, $target);//загрузка оригинала в папку $path_to_90_directory

        if(preg_match('/[.](GIF)|(gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_90_directory.$filename) ; //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if(preg_match('/[.](PNG)|(png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_90_directory.$filename) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_90_directory.$filename); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        $w = 140;
        $h = 121;  // квадратная 90x90. Можно поставить и другой размер.

// создаём исходное изображение на основе
// исходного файла и определяем его размеры
        $w_src = imagesx($im); // определяем ширину
        $h_src = imagesy($im); // определяем высоту изображения

        // создаём пустую квадратную картинку
        // важно именно truecolor!, иначе будем иметь 8-битный результат
        $dest = imagecreatetruecolor($w,$h);

        // вырезаем квадратную серединку по x, если фото горизонтальное
        if ($w_src == 1 || $h_src == 3) {
            imagealphablending($dest, true);
            imageSaveAlpha($dest, true);
            $transparent = imagecolorallocatealpha($dest, 0, 0, 0, 127);
            imagefill($dest, 0, 0, $transparent);
            imagecolortransparent($dest, $transparent);
        }


        $tw = ceil($h / ($h_src  / $w_src));
        $th = ceil($w / ($w_src / $h_src));
        if ($tw > $w) {
            imageCopyResampled($dest, $im, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $w_src, $h_src);
        } else {
            imageCopyResampled($dest, $im, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $w_src, $h_src);
        }



        $date = date("dmyi"); //вычисляем время в настоящий момент.


        imagejpeg($dest, $path_to_90_directory.$date.".jpg"); //сохраняем изображение формата jpg в нужную папку, именем будет текущее время. Сделано, чтобы у изображений не было одинаковых названий.

//почему именно jpg? Он занимает очень мало места + уничтожается анимирование gif изображения, которое отвлекает пользователя. Не очень приятно читать его комментарий, когда краем глаза замечаешь какое-то движение.

        $avatar = $path_to_90_directory.$date.".jpg"; //заносим в переменную путь до аватара.

        $delfull = $path_to_90_directory.$filename; // получаем адрес исходника
        unlink ($delfull); //удаляем оригинал загруженного изображения, он нам больше не нужен. Задачей было - получить миниатюру.
    }
}
function add_image2 ()
{
    $upload_photo = $_FILES['image2']['name']; // загружаемый из формы из поля upload_photo файл

    $path_to_90_directory = 'images/slide/';//папка, куда будет загружаться начальная картинка и ее сжатая копия

    if(preg_match('/[.](JPG)|(jpg)|(gif)|(GIF)|(png)|(PNG)$/',$_FILES['image2']['name']))//проверка формата исходного изображения
    {

        $filename = $_FILES['image2']['name'];
        $source = $_FILES['image2']['tmp_name'];
        $target = $path_to_90_directory . $filename;
        move_uploaded_file($source, $target);//загрузка оригинала в папку $path_to_90_directory

        if(preg_match('/[.](GIF)|(gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_90_directory.$filename) ; //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if(preg_match('/[.](PNG)|(png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_90_directory.$filename) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_90_directory.$filename); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        $w = 620;
        $h = 403;  // квадратная 90x90. Можно поставить и другой размер.

// создаём исходное изображение на основе
// исходного файла и определяем его размеры
        $w_src = imagesx($im); // определяем ширину
        $h_src = imagesy($im); // определяем высоту изображения

        // создаём пустую квадратную картинку
        // важно именно truecolor!, иначе будем иметь 8-битный результат
        $dest = imagecreatetruecolor($w,$h);

        // вырезаем квадратную серединку по x, если фото горизонтальное
        if ($w_src == 1 || $h_src == 3) {
            imagealphablending($dest, true);
            imageSaveAlpha($dest, true);
            $transparent = imagecolorallocatealpha($dest, 0, 0, 0, 127);
            imagefill($dest, 0, 0, $transparent);
            imagecolortransparent($dest, $transparent);
        }


        $tw = ceil($h / ($h_src  / $w_src));
        $th = ceil($w / ($w_src / $h_src));
        if ($tw > $w) {
            imageCopyResampled($dest, $im, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $w_src, $h_src);
        } else {
            imageCopyResampled($dest, $im, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $w_src, $h_src);
        }



        $date = date("dmyi"); //вычисляем время в настоящий момент.


        imagejpeg($dest, $path_to_90_directory.$date.".jpg"); //сохраняем изображение формата jpg в нужную папку, именем будет текущее время. Сделано, чтобы у изображений не было одинаковых названий.

//почему именно jpg? Он занимает очень мало места + уничтожается анимирование gif изображения, которое отвлекает пользователя. Не очень приятно читать его комментарий, когда краем глаза замечаешь какое-то движение.

        $avatar = $path_to_90_directory.$date.".jpg"; //заносим в переменную путь до аватара.

        $delfull = $path_to_90_directory.$filename; // получаем адрес исходника
        unlink ($delfull); //удаляем оригинал загруженного изображения, он нам больше не нужен. Задачей было - получить миниатюру.
    }
}