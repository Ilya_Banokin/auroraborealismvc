<?php
require_once 'admin/module/aDesign.php';
function categories_drink () {
    $db = new Db();

    $params = [
        'id' => 5,
    ];

    $catDrink = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catDrink;
}
function get_posts_drink () {
    $db = new Db();

    $postDrink = $db->row("SELECT * FROM `drink`");

    return $postDrink;
}
function articles_get () {
    $db = new Db();

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $article = $db->row("SELECT * FROM `drink` WHERE `id`= :id",$params);

    return $article;
}
function articles_new()
{
    $db = new Db();

    $params = [
        'title' => $_POST['title'],
        'meta_d' => $_POST['meta_d'],
        'description' => $_POST['description'],
        'text' => $_POST['text'],
        'mini_img' => date("dmyi").'.jpg',
        'image' => date("dmyi").'.jpg',
        'alt' => $_POST['alt'],
        'weight' => $_POST['weight'],
        'price' => $_POST['price'],
        'view' => 0,
    ];

    $db->query("INSERT INTO `drink` (`title`,`meta_d`,`description`,`text`,`mini_img`,`image`,`alt`,`weight`,`price`,`view`) VALUES (:title,:meta_d,:description,:text,:mini_img,:image,:alt,:weight,:price,:view)",$params);
}
function articles_edit() {
    $db = new Db();

    $params = [
        'title' => $_POST['title'],
        'meta_d' => $_POST['meta_d'],
        'description' => $_POST['description'],
        'text' => $_POST['text'],
        'mini_img' => $_POST['mini_img'],
        'image' => $_POST['image'],
        'alt' => $_POST['alt'],
        'weight' => $_POST['weight'],
        'price' => $_POST['price'],
        'view' => (int)$_POST['view'],
        'id' => (int)$_GET['id'],
    ];

    $db->query("UPDATE `drink` SET `title` = :title,`meta_d` = :meta_d,`description` = :description,`text` = :text,`mini_img` = :mini_img,`image` = :image,`alt` = :alt,`weight` = :weight,`price` = :price,`view` = :view WHERE `id` = :id",$params);
}
function articles_delete(){
    $db = new Db();

    if ($_GET['id'] == 0) exit('ID не найден');

    $params = [
        'id' => $_GET['id'],
    ];

    $db->query("DELETE FROM `drink` WHERE `id`= :id",$params);
}
function add_image ()
{
    $upload_photo = $_FILES['image']['name']; // загружаемый из формы из поля upload_photo файл

    $path_to_90_directory = 'images/cat-tovar/drink/mini_drink/';//папка, куда будет загружаться начальная картинка и ее сжатая копия

    if(preg_match('/[.](JPG)|(jpg)|(gif)|(GIF)|(png)|(PNG)$/',$_FILES['image']['name']))//проверка формата исходного изображения
    {

        $filename = $_FILES['image']['name'];
        $source = $_FILES['image']['tmp_name'];
        $target = $path_to_90_directory . $filename;
        move_uploaded_file($source, $target);//загрузка оригинала в папку $path_to_90_directory

        if(preg_match('/[.](GIF)|(gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_90_directory.$filename) ; //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if(preg_match('/[.](PNG)|(png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_90_directory.$filename) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_90_directory.$filename); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        $w = 250;
        $h = 127;  // квадратная 90x90. Можно поставить и другой размер.

// создаём исходное изображение на основе
// исходного файла и определяем его размеры
        $w_src = imagesx($im); // определяем ширину
        $h_src = imagesy($im); // определяем высоту изображения

        // создаём пустую квадратную картинку
        // важно именно truecolor!, иначе будем иметь 8-битный результат
        $dest = imagecreatetruecolor($w,$h);

        // вырезаем квадратную серединку по x, если фото горизонтальное
        if ($w_src == 1 || $h_src == 3) {
            imagealphablending($dest, true);
            imageSaveAlpha($dest, true);
            $transparent = imagecolorallocatealpha($dest, 0, 0, 0, 127);
            imagefill($dest, 0, 0, $transparent);
            imagecolortransparent($dest, $transparent);
        }


        $tw = ceil($h / ($h_src  / $w_src));
        $th = ceil($w / ($w_src / $h_src));
        if ($tw > $w) {
            imageCopyResampled($dest, $im, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $w_src, $h_src);
        } else {
            imageCopyResampled($dest, $im, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $w_src, $h_src);
        }



        $date = date("dmyi"); //вычисляем время в настоящий момент.


        imagejpeg($dest, $path_to_90_directory.$date.".jpg"); //сохраняем изображение формата jpg в нужную папку, именем будет текущее время. Сделано, чтобы у изображений не было одинаковых названий.

//почему именно jpg? Он занимает очень мало места + уничтожается анимирование gif изображения, которое отвлекает пользователя. Не очень приятно читать его комментарий, когда краем глаза замечаешь какое-то движение.

        $avatar = $path_to_90_directory.$date.".jpg"; //заносим в переменную путь до аватара.

        $delfull = $path_to_90_directory.$filename; // получаем адрес исходника
        unlink ($delfull); //удаляем оригинал загруженного изображения, он нам больше не нужен. Задачей было - получить миниатюру.
    }
}
function add_image2 ()
{
    $upload_photo = $_FILES['image2']['name']; // загружаемый из формы из поля upload_photo файл

    $path_to_90_directory = 'images/cat-tovar/drink/';//папка, куда будет загружаться начальная картинка и ее сжатая копия

    if(preg_match('/[.](JPG)|(jpg)|(gif)|(GIF)|(png)|(PNG)$/',$_FILES['image2']['name']))//проверка формата исходного изображения
    {

        $filename = $_FILES['image2']['name'];
        $source = $_FILES['image2']['tmp_name'];
        $target = $path_to_90_directory . $filename;
        move_uploaded_file($source, $target);//загрузка оригинала в папку $path_to_90_directory

        if(preg_match('/[.](GIF)|(gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_90_directory.$filename) ; //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if(preg_match('/[.](PNG)|(png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_90_directory.$filename) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_90_directory.$filename); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }

        $w = 350;
        $h = 230;  // квадратная 90x90. Можно поставить и другой размер.

// создаём исходное изображение на основе
// исходного файла и определяем его размеры
        $w_src = imagesx($im); // определяем ширину
        $h_src = imagesy($im); // определяем высоту изображения

        // создаём пустую квадратную картинку
        // важно именно truecolor!, иначе будем иметь 8-битный результат
        $dest = imagecreatetruecolor($w,$h);

        // вырезаем квадратную серединку по x, если фото горизонтальное
        if ($w_src == 1 || $h_src == 3) {
            imagealphablending($dest, true);
            imageSaveAlpha($dest, true);
            $transparent = imagecolorallocatealpha($dest, 0, 0, 0, 127);
            imagefill($dest, 0, 0, $transparent);
            imagecolortransparent($dest, $transparent);
        }


        $tw = ceil($h / ($h_src  / $w_src));
        $th = ceil($w / ($w_src / $h_src));
        if ($tw > $w) {
            imageCopyResampled($dest, $im, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $w_src, $h_src);
        } else {
            imageCopyResampled($dest, $im, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $w_src, $h_src);
        }



        $date = date("dmyi"); //вычисляем время в настоящий момент.


        imagejpeg($dest, $path_to_90_directory.$date.".jpg"); //сохраняем изображение формата jpg в нужную папку, именем будет текущее время. Сделано, чтобы у изображений не было одинаковых названий.

//почему именно jpg? Он занимает очень мало места + уничтожается анимирование gif изображения, которое отвлекает пользователя. Не очень приятно читать его комментарий, когда краем глаза замечаешь какое-то движение.

        $avatar = $path_to_90_directory.$date.".jpg"; //заносим в переменную путь до аватара.

        $delfull = $path_to_90_directory.$filename; // получаем адрес исходника
        unlink ($delfull); //удаляем оригинал загруженного изображения, он нам больше не нужен. Задачей было - получить миниатюру.
    }
}