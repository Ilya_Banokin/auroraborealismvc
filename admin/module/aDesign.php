<?php
function head()
{
    if ($_SERVER['REQUEST_URI'] == '/admin/index' or $_SERVER['REQUEST_URI'] == '/admin/') $atitle = 'Главная станица';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/amenu') $atitle = 'Меню ресторана';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/asalad') $atitle = 'Салаты';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/asoup') $atitle = 'Супы';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/ahot') $atitle = 'Горячие блюда';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/asnack') $atitle = 'Закуски';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/adrink') $atitle = 'Напитки';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/apricing') $atitle = 'Прайс Лист';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/anews') $atitle = 'Новости';
    elseif ($_SERVER['REQUEST_URI'] == '/admin/aslider') $atitle = 'Slider';
echo '
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>СЕВЕРНОЕ СИЯНИЕ | Панель администратора | '; echo $atitle; echo '</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
  <script src="ckeditor/ckeditor.js"></script>
</head>
  <body class="grey lighten-3">
  <header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">
        <a href="#" class="navbar-brand waves-effect">
          <strong class="blue-text">Добро пожаловать <b>Admin</b></strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Togglenavigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a href="index" class="nav-link waves-effect">Главная</a>
            </li>
            <li class="nav-item"><a href="amenu" class="nav-link waves-effect">Меню ресторана</a>
            </li>
            <li class="nav-item"><a href="apricing" class="nav-link waves-effect">Прайс-лист</a>
            </li>
            <li class="nav-item"><a href="#" class="nav-link waves-effect">Доставка</a>
            </li>
            <li class="nav-item"><a href="#" class="nav-link waves-effect">Контакты</a>
            </li>
          </ul>
          <ul class="navbar-nav nav-flex-icons">
            <a href="https://vk.com/auroraborealisspb" target="_blank">
            <li class="nav-item mr-2">
                <img src="img/icon/vk.png" alt="Вконтакте">
            </li></a>
            <a href="#">
            <li class="nav-item mr-2">
                <img src="img/icon/inst.png" alt="Инстаграм">
            </li></a>
          </ul>
        </div>
      </div>
    </nav>
    <div class="sidebar-fixed position-fixed">
        <a href="#" class="logo-wrapper waves-effect">
          <img src="img/logo.png" alt="" class="img-fluid"></img>
        </a>

        <div class="list-group list-group-flush">
          <a href="aslider" class="list-group-item waves-effect">
            <i class="fa fa-pie-chart mr-3"><img src="img/icon/slide.png" width="30px" height="30px"/></i>Слайдер
          </a>
          <a href="anews" class="list-group-item waves-effect">
            <i class="fa fa-pie-user mr-3"><img src="img/icon/news.png" width="30px" height="30px"/></i>Новости
          </a>
          <a href="amenu" class="list-group-item waves-effect">
            <i class="fa fa-pie-chart mr-3"><img src="img/icon/menu.png" width="30px" height="30px"/></i>Меню ресторана
          </a>
          <a href="apricing" class="list-group-item waves-effect">
            <i class="fa fa-pie-chart mr-3"><img src="img/icon/price.png" width="30px" height="30px"/></i>Прайс-лист
          </a>

      </div>
  </header>
  
';
}

function afooter ()
{
    if ($_SERVER['REQUEST_URI'] == '/admin/index' or $_SERVER['REQUEST_URI'] == '/admin/' or $_SERVER['REQUEST_URI'] == '/admin/anews'){
echo '
<footer class="fixed-bottom page-footer font-small blue">
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://vk.com/banokin73"> by ILYA Banokin v.3.0</a>
  </div>
    </footer>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>
</html>
';}else {
        echo '
        <footer class="bottom page-footer font-small blue">
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://vk.com/banokin73"> by ILYA Banokin v.3.0</a>
  </div>
    </footer>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>
</html>
        ';
    }
}