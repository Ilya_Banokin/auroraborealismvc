<?php
require_once 'admin/module/modNews.php';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}else {
    $action = "";
}


if ($action == "add")
{
    if(!empty($_POST)) {
        articles_new();
        header("Location: anews");
    }
    include 'admin/forms/fnews.php';
}
else if($action == "edit")
{
    if(!isset($_GET['id']))
    {
        exit('ID не найден');
    }elseif (!empty($_POST) && $_GET['id'] > 0) {
        articles_edit();
        header("Location: anews");
    }
    require_once 'admin/forms/fnews.php';
}
else if($action == "delete")
{
    $article = articles_delete();
    header("Location: anews");
}
else {
    include 'admin/news/page/aNews.php';
}
