<?php
include 'module/aDesign.php';
head();?>


    <main class="pt-5 max-lg-5">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="#">Главная страница</a>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" class="form-control" placeholder="Быстрый поиск">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                        </input>
                    </form>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Card deck -->
            <div class="card-deck">

                <!-- Card -->
                <div class="card mb-4 poligon-main">

                    <!--Card image-->
                    <div class="view overlay">
                        <img class="card-img-top" src="img/menu/menu.jpg" alt="">
                        <a href="amenu">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>

                    <!--Card content-->
                    <div class="card-body">

                        <!--Title-->
                        <h4 class="card-title">МЕНЮ РЕСТОРАНА</h4>
                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <a href="amenu"><button type="button" class="btn btn-light-blue btn-md">Редактор категории</button></a>

                    </div>

                </div>
                <!-- Card -->

                <!-- Card -->
                <div class="card mb-4 poligon-main">

                    <!--Card image-->
                    <div class="view overlay">
                        <img class="card-img-top" src="img/menu/slide.png" alt="Прайс лист Северное Сияние">
                        <a href="aslider">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>

                    <!--Card content-->
                    <div class="card-body">

                        <!--Title-->
                        <h4 class="card-title">СЛАЙДЕР</h4>
                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <button type="button" class="btn btn-light-blue btn-md">Редактор категории</button>

                    </div>

                </div>
                <!-- Card -->

                <!-- Card -->
                <div class="card mb-4 poligon-main">

                    <!--Card image-->
                    <div class="view overlay">
                        <img class="card-img-top" src="img/news/news.jpg">
                        <a href="anews">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>

                    <!--Card content-->
                    <div class="card-body">

                        <!--Title-->
                        <h4 class="card-title">НОВОСТИ</h4>
                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <button type="button" class="btn btn-light-blue btn-md">Редактор категории</button>

                    </div>

                </div>
                <!-- Card -->


                <!-- Card -->
                <div class="card mb-4 poligon-main">

                    <!--Card image-->
                    <div class="view overlay">
                        <img class="card-img-top" src="img/price/price.png" alt="Прайс лист Северное Сияние">
                        <a href="apricing">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>

                    <!--Card content-->
                    <div class="card-body">
                        <!--Title-->
                        <h4 class="card-title">ПРАЙС ЛИСТ</h4>
                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <a href="apricing"><button type="button" class="btn btn-light-blue btn-md">Редактор категории</button></a>
                    </div>
                </div>
            </div>
        </div>
    </main>



<?php afooter();?>