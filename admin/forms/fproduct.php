<?php
if ($module == 'asalad') require_once 'admin/module/menu/modSalad.php';
elseif ($module == 'asoup') require_once 'admin/module/menu/modSoup.php';
elseif ($module == 'ahot') require_once 'admin/module/menu/modHot.php';
elseif ($module == 'asnack') require_once 'admin/module/menu/modSnack.php';
elseif ($module == 'adrink') require_once 'admin/module/menu/modDrink.php';
elseif ($module == 'agarnish') require_once 'admin/module/menu/modGarnish.php';
elseif ($module == 'apizza') require_once 'admin/module/menu/modPizza.php';
$article = articles_get();
head();?>
    <main class="pt-5 max-lg-5">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="index">Главная страница</a>
                        <span> / </span>
                        <a href="amenu"><span>Меню Ресторана</span></a>
                        <span> / </span>
                        <?php
                        if ($module == 'asalad') {
                            echo "<a href='asalad'><span>Салаты</span></a>";
                            }elseif ($module == 'asoup') {
                            echo "<a href='asoup'><span>Супы</span></a>";
                        }elseif ($module == 'ahot') {
                            echo "<a href='ahot'><span>Горячие блюда</span></a>";
                        }elseif ($module == 'asnack') {
                            echo "<a href='asnack'><span>Закуски</span></a>";
                        }elseif ($module == 'adrink') {
                            echo "<a href='adrink'><span>Напитки</span></a>";
                        }elseif ($module == 'agarnish') {
                            echo "<a href='agarnish'><span>Гарниры</span></a>";
                        }elseif ($module == 'apizza') {
                            echo "<a href='apizza'><span>Пицца</span></a>";
                        }
                        ?>
                        <span> / </span>
                        <span>
                        <?php
                        if($action == 'add') {
                            echo 'Новый продукт';
                        }else{
                            echo 'Редактировать продукт';
                        }
                        ?>
                    </span>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" class="form-control" placeholder="Быстрый поиск">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                        </input>
                    </form>
                </div>
            </div>
        </div>
        <form method="post" action="<?php
        if ($module == 'asalad') echo 'asalad';
        elseif ($module == 'asoup') echo 'asoup';
        elseif ($module == 'ahot') echo 'ahot';
        elseif ($module == 'apizza') echo 'apizza';
        elseif ($module == 'asnack') echo 'asnack';
        elseif ($module == 'adrink') echo 'adrink';
        elseif ($module == 'agarnish') echo 'agarnish';?>?action=<?=$_GET['action']?>&id=<?=(int)$_GET['id']?>" class="form" enctype="multipart/form-data">
            <div class="col-6">
                <p class="admin-warning"><b> * - Поля обязательные к заполнению!</b></p>
                <div class="form-group">
                    <label for="formGroupExampleInput">Название продукта: *</label>
                    <input type="text" class="form-control" name="title" id="formGroupExampleInput"
                           value="<?php if($action == 'edit') {
                               foreach ($article as $a) echo $a['title'];}?>" placeholder="Введите название">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Ключевые фразы:</label>
                    <input type="text" class="form-control" name="meta_d" value="<?php if($action == 'edit') {
                        foreach ($article as $a) echo $a['meta_d'];}else {echo ' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге';}?>"
                           id="formGroupExampleInput2" placeholder="Meta_description">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Описание категории: *</label>
                    <textarea name="description" id="editor1" rows="4" cols="36"><?php if($action == 'edit') {
                            foreach ($article as $a) echo $a['description'];}?></textarea>
                </div>

                <script>
                    CKEDITOR.replace('editor1');
                </script>

                <div class="form-group">
                    <label for="formGroupExampleInput2">Текст: *</label>
                    <textarea name="text" id="editor2" rows="4" cols="36"><?php if($action == 'edit') {
                            foreach ($article as $a) echo $a['text'];}?></textarea>
                </div>

                <script>
                    CKEDITOR.replace( 'editor2' );
                </script>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="customFileLang">
                        <label class="custom-file-label" for="customFileLang">Выбрать изображение</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image2" id="customFileLang">
                        <label class="custom-file-label" for="customFileLang">Выбрать изображение: (на странице товара)</label>
                    </div>
                </div>

                <?php
                foreach ($article as $a) {
                    $mimage = $a['mini_img'];
                    $image = $a['image'];
                }
                if ($action == 'edit')
                {
                    echo "
                        <div class='form-group'>
                    <label for='formGroupExampleInput2'>Имя файла:</label>
                    <input type='text' class='form-control' name='mini_img' id='formGroupExampleInput2'  value='$mimage'>
                </div>

                <div class='form-group'>
                    <label for='formGroupExampleInput2'>Имя файла (на странице товара):</label>
                    <input type='text' class='form-control' name='image' id='formGroupExampleInput2'  value='$image' placeholder='Формат: д.м.г.мин.'>
                </div>
                    ";
                }

                ?>

                <div class="form-group">
                    <label for="formGroupExampleInput2">Подпись к изображению (ALT):</label>
                    <input type="text" class="form-control" name="alt" id="formGroupExampleInput2" value="<?php if($action == 'edit') {
                        foreach ($article as $a) {
                            echo $a['alt'];
                        }
                    } ?>"></div>


                <div class="form-group">
                    <label for="formGroupExampleInput2">Вес (гр.):</label>
                    <input type="text" class="form-control" name="weight" id="formGroupExampleInput2" value="<?php if($action == 'edit') {
                        foreach ($article as $a) {
                            echo $a['weight'];
                        }
                    } ?>"></div>

                <div class="form-group">
                    <label for="formGroupExampleInput2">Цена: *</label>
                    <input type="text" class="form-control" name="price" value="<?php if($action == 'edit') {
                        foreach ($article as $a) echo $a['price'];} ?>" id="formGroupExampleInput2" placeholder="Цена за продукт">
                </div>
                <input type="hidden" name="view" value="<?php if($action == 'edit') {
                    foreach ($article as $a) echo $a['view'];} ?>">

                <button type="submit" class="btn btn-outline-primary waves-effect">Сохранить</button>
                <a href="<?php
                if ($module == 'amenu') echo 'amenu';
                elseif ($module == 'asalad') echo 'asalad';
                elseif ($module == 'asoup') echo 'asoup';
                elseif ($module == 'ahot') echo 'ahot';
                elseif ($module == 'apizza') echo 'apizza';
                elseif ($module == 'asnack') echo 'asnack';
                elseif ($module == 'adrink') echo 'adrink';
                elseif ($module == 'agarnish') echo 'agarnish';
                ?>" class="btn btn-outline-primary waves-effect">Отмена</a>
            </div>
        </form>
    </main>

<?php
afooter(); ?>