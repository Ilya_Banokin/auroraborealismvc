<?php
require_once 'admin/module/modNews.php';
$article = articles_get();
head();
?>
    <main class="pt-5 max-lg-5">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="index">Главная страница</a>
                        <span> / </span>
                        <a href="anews"><span>Новости</span></a>
                        <span> / </span>
                        <span>
                        <?php
                        if($action == 'add'){
                            echo 'Новая новость';
                        }else{
                            echo 'Редактировать новость';
                        }?>
                    </span>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" class="form-control" placeholder="Быстрый поиск">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                        </input>
                    </form>
                </div>
            </div>
        </div>
        <form method="post" action="anews?action=<?=$_GET['action']?>&id=<?=(int)$_GET['id']?>" class="form"
              enctype="multipart/form-data">
            <div class="col-6">
                <p class="admin-warning"><b> * - Поля обязательные к заполнению!</b></p>

                    <input type="hidden" class="form-control" name="date" id="formGroupExampleInput"
                           value="<?php if($action == 'edit') {
                               foreach ($article as $a) {
                                   echo $a['date'];
                               }
                           } ?>">

                <div class="form-group">
                    <label for="formGroupExampleInput2">Текст: *</label>
                    <textarea name="text" id="editor1" rows="4" cols="36"><?php if($action == 'edit') {
                            foreach ($article as $a) {
                                echo $a['text'];
                            }
                        } ?></textarea>
                </div>

                <script>
                    CKEDITOR.replace('editor1');
                </script>

                <button type="submit" class="btn btn-outline-primary waves-effect">Сохранить</button>
                <a href="anews" class="btn btn-outline-primary waves-effect">Отмена</a>
            </div>
        </form>
    </main>

<?php
afooter(); ?>