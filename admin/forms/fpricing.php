<?php
require_once 'admin/module/modPricing.php';
$article = articles_get();
head();
?>
    <main class="pt-5 max-lg-5">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="index">Главная страница</a>
                        <span> / </span>
                        <a href="amenu"><span>Меню Ресторана</span></a>
                        <span> / </span>
                        <span>
                        <?php
                        if($action == 'add'){
                            echo 'Новая категория';
                        }else{
                        echo 'Редактировать категорию';
                        }?>
                    </span>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" class="form-control" placeholder="Быстрый поиск">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                        </input>
                    </form>
                </div>
            </div>
        </div>
        <form method="post" action="apricing?action=<?=$_GET['action']?>&id=<?=(int)$_GET['id']?>" class="form"
              enctype="multipart/form-data">
            <div class="col-6">
                <p class="admin-warning"><b> * - Поля обязательные к заполнению!</b></p>
                <div class="form-group">
                    <label for="formGroupExampleInput">Название продукта: *</label>
                    <input type="text" class="form-control" name="name" id="formGroupExampleInput"
                           value="<?php if($action == 'edit') {
                               foreach ($article as $a) {
                                   echo $a['name'];
                               }
                           } ?>" placeholder="Введите название">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Ключевые фразы:</label>
                    <input type="text" class="form-control" name="meta_d" value="<?php if($action == 'edit') {
                        foreach ($article as $a) {
                            echo $a['meta_d'];
                        }
                    } ?>"
                           id="formGroupExampleInput2" placeholder="Meta_description">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Описание категории: *</label>
                    <textarea name="text" id="editor1" rows="4" cols="36"><?php if($action == 'edit') {
                            foreach ($article as $a) {
                                echo $a['text'];
                            }
                        } ?></textarea>
                </div>

                <script>
                    CKEDITOR.replace('editor1');
                </script>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="customFileLang">
                        <label class="custom-file-label" for="customFileLang">Выбрать изображение</label>
                    </div>
                </div>

                <?php
                if ($action == 'edit')
                {
                    foreach ($article as $a) {
                        $img = $a['image'];
                    }
                    echo "
                <div class='form-group'>
                    <label for='formGroupExampleInput2'>Имя файла:</label>
                    <input type='text' class='form-control' name='image' id='formGroupExampleInput2'  value='$img' placeholder='Формат: д.м.г.мин.'>
                </div>
                ";
                }
                ?>

                <div class="form-group">
                    <label for="formGroupExampleInput2">Подпись к изображению (ALT):</label>
                    <input type="text" class="form-control" name="alt" id="formGroupExampleInput2" value="<?php if($action == 'edit') {
                        foreach ($article as $a) {
                            echo $a['alt'];
                        }
                    } ?>"></div>

                <div class="form-group">
                    <label for="formGroupExampleInput2">Фасовка:</label>
                    <input type="text" class="form-control" name="weight" id="formGroupExampleInput2" value="<?php if($action == 'edit') {
                        foreach ($article as $a) {
                            echo $a['weight'];
                        }
                    } ?>"></div>

                <div class="form-group">
                    <label for="formGroupExampleInput2">Цена:</label>
                    <input type="text" class="form-control" name="price" id="formGroupExampleInput2" value="<?php if($action == 'edit') {
                        foreach ($article as $a) {
                            echo $a['price'];
                        }
                    } ?>"></div>

                <input type="hidden" name="view" value="<?php if($action == 'edit') {
                    foreach ($article as $a) echo $a['view'];} ?>">

                <button type="submit" class="btn btn-outline-primary waves-effect">Сохранить</button>
                <a href="apricing" class="btn btn-outline-primary waves-effect">Отмена</a>
            </div>
        </form>
    </main>

<?php
afooter(); ?>