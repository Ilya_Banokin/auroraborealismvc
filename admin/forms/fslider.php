<?php
require_once 'admin/module/modSlider.php';
$article = articles_get();
head();
?>
    <main class="pt-5 max-lg-5">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="index">Главная страница</a>
                        <span> / </span>
                        <a href="aslider"><span>Слайдер</span></a>
                        <span> / </span>
                        <span>Редактировать слайд</span>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" class="form-control" placeholder="Быстрый поиск">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                        </input>
                    </form>
                </div>
            </div>
        </div>
        <form method="post" action="aslider?action=<?=$_GET['action']?>&id=<?=(int)$_GET['id']?>" class="form"
              enctype="multipart/form-data">
            <div class="col-6">

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="customFileLang">
                        <label class="custom-file-label" for="customFileLang">Миниатюра</label>
                    </div>
                </div>
                <?php
                foreach ($article as $a) {
                    $mimage = $a['mini_image'];
                    $image = $a['image'];
                }
                ?>
                <input type='hidden' class='form-control' name='mini_image' value='<?=$mimage?>'>
                <input type='hidden' class='form-control' name='image'  value='<?=$image?>'>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image2" id="customFileLang">
                        <label class="custom-file-label" for="customFileLang">Изображение</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-outline-primary waves-effect">Сохранить</button>
                <a href="aslider" class="btn btn-outline-primary waves-effect">Отмена</a>
            </div>
        </form>
    </main>

<?php
afooter(); ?>