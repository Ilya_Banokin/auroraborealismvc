<?php
require_once 'admin/module/menu/modSoup.php';

if (isset($_FILES['image']['name'])) {
    add_image ();
}
if (isset($_FILES['image2']['name'])) {
    add_image2 ();
}

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}
else {
    $action = "";
}
if ($action == "add") {
    if(!empty($_POST)) {
        articles_new();
        header("Location: asoup");
    }
    require_once 'admin/forms/fproduct.php';
}

else if($action == "edit"){
    if(!isset($_GET['id']))
    {
        exit('ID не найден');
    }elseif (!empty($_POST) && $_GET['id'] > 0) {
        articles_edit();
        header("Location: asoup");
    }
    require_once 'admin/forms/fproduct.php';
}
else if($action == "delete"){
    $article = articles_delete();
    header("Location: asoup");
}
else {
    include 'admin/menu/pages/aSoup.php';
}
?>