<?php
$catDrink = categories_drink ();
head();
?>

    <main class="pt-5 max-lg-5">
        <div class="container-fluid mt-5">
            <div class="card mb-4 wow fadeIn">
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="index">Главная страница</a>
                        <span> / </span>
                        <a href="amenu"><span>Меню Ресторана</span></a>
                        <span> / </span>
                        <span><?php
                            foreach ($catDrink as $drinkTitle)
                            {
                                echo $drinkTitle['title'];
                            }
                            ?></span>
                    </h4>
                    <form class="d-flex justify-content-center">
                        <input type="search" class="form-control" placeholder="Быстрый поиск">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                        </input>
                    </form>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Editable table -->
            <div class="card">
                <div class="card-body">
                    <div id="table" class="table-editable">
                    <span class="table-add float-right mb-3 mr-2"><a href="adrink?action=add" class="text-success"><i
                                class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                        <table class="table table-bordered table-responsive-md table-striped text-center">
                            <thead>
                            <tr>
                                <th class="text-center">НАЗВАНИЕ</th>
                                <th class="text-center">ОПИСАНИЕ</th>
                                <th class="text-center">ТЕКСТ</th>
                                <th class="text-center">ЦЕНА</th>
                                <th class="text-center"><img src="/admin/img/icon/view.jpg" alt=""/> </th>
                                <th class="text-center">РЕДАКТИРОВАТЬ</th>
                                <th class="text-center">УДАЛИТЬ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $postDrink = get_posts_drink ();
                            foreach ($postDrink as $cat): ?>
                                <tr>
                                    <td class="pt-3-half"><?=$cat['title']?></td>
                                    <td class="pt-3-half"><?=$cat['description']?></td>
                                    <td class="pt-3-half"><?=$cat['text']?></td>
                                    <td class="pt-3-half"><?=$cat['price']?></td>
                                    <td class="pt-3-half"><?=$cat['view']?></td>
                                    <td class="pt-3-half">
              <span class="table-remove">
                  <a href="adrink?action=edit&id=<?=$cat['id']; ?>">
                    <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">Редактировать</button>
                  </a>
              </span>
                                    </td>
                                    <td>
              <span class="table-remove">
                  <a href="adrink?action=delete&id=<?=$cat['id']; ?>">
                    <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">Удалить</button>
                  </a>
              </span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Editable table -->
        </div>


    </main>



<?php afooter(); ?>