<?php
require_once 'admin/module/modMenu.php';

if (isset($_FILES['imagecategories']['name'])) {
    add_image_categories();
}

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}
else {
    $action = "";
}


if ($action == "add")
{
    if(!empty($_POST)) {
        articles_new();
        header("Location: amenu");
    }
    include 'admin/forms/fmenu.php';
}
else if($action == "edit")
{
    if(!isset($_GET['id']))
    {
        exit('ID не найден');
    }elseif (!empty($_POST) && $_GET['id'] > 0) {
        articles_edit();
        header("Location: amenu");
    }
    require_once 'admin/forms/fmenu.php';
}
else if($action == "delete")
{
    $article = articles_delete();
    header("Location: amenu");
}
else {
    include 'admin/menu/pages/aMenu.php';
}
