<?php
require_once 'module/pricing.php';
top();?>

    <!-- content -->
    <section id="content">
        <div class="bg-top">
            <div class="bg-top-2">
                <div class="bg">
                    <div class="bg-top-shadow">
                        <div class="main">
                            <div class="box p3">
                                <div class="padding">
                                    <div class="container_12">
                                        <div class="wrapper">
                                            <div class="grid_12">
                                                <div class="wrapper">
                                                    <?php
                                                    $page = isset($_GET['page']) ? $_GET['page'] : 1;
                                                    $limit = 25;
                                                    $offset = $limit * ($page - 1);
                                                    $postPricing = get_posts_price ($limit,$offset);
                                                    ?>
                                                    <table>
                                                        <thead>
                                                        <tr>
                                                            <td align="center">Наименование</td>
                                                            <td align="center">Фасовка</td>
                                                            <td align="center">Цена</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($postPricing as $prices): ?>
                                                        <tr>
                                                            <td><a href="pricing/post_pricing?id=<?=$prices['id'];?>"><?=$prices['name']?></a></td>
                                                            <td align="center"><a href="post_pricing?id=<?=$prices['id'];?>"><?=$prices['weight']?></a></td>
                                                            <td align="center"><a href="post_pricing?id=<?=$prices['id'];?>"><?=$prices['price']?> Руб.</a></td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                    <br />
                                                    <!-- НАЧАЛО ПАГИНАЦИИ -->
                                                    <div class="paginate wrapper">
                                                        <?php
                                                        $db = new Db();

                                                        $result = $db->column("SELECT COUNT(*) FROM `pricing`");

                                                        $pagesCount = ceil($result / $limit);
                                                        // ЕСЛИ СТРАНИЦА МЕНЬШЕ ЛИБО РАВНА 1, ТО ВЫВЕДИ ЗНАК PREV
                                                        if ($page != 1) {
                                                            $prev = $page - 1;
                                                            echo "<a href=\"?page=$prev\">&lang;</a>";
                                                        }
                                                        // ВЫВОД ПАГИНАТОРА ЧЕРЕЗ ЦИКЛ
                                                        for ($i = 1; $i <= $pagesCount; $i++) {
                                                            if ($page == $i) {
                                                                $class = ' class="active"';
                                                            }else {
                                                                $class = '';
                                                            }
                                                            echo "<a href=\"?page=$i\"$class>$i</a>";
                                                        }
                                                        // ЕСЛИ НА СТРАНИЦЕ ЗАПИСИЕЙ СКОЛЬКО В $limit, ТО ВЫВОДИ ЗНАК NEXT
                                                        if ($page < $pagesCount) {
                                                            $next = $page + 1;
                                                            echo "<a href=\"?page=$next\">&rang;</a>";
                                                        }
                                                        ?>
                                                    </div>
                                                    <!-- КОНЕЦ ПАГИНАЦИИ -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php bottom();
footer();?>