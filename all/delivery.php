<?php
require_once 'module/delivery.php';
top('Доставка');?>


    <!-- content -->
    <section id="content">
        <div class="bg-top">
            <div class="bg-top-2">
                <div class="bg">
                    <div class="bg-top-shadow">
                        <div class="main">
                            <div class="gallery p3">
                                <p class="hdostav">Условия оплаты и доставки <img class="imgstr" src="images/deliv-image/str.png" alt="strelka" /></p>
                                <fieldset class="dleverytaingun">
                                    <legend class="mealitykadsets">
                              <span class="stryconnectivi"><p class="title_del">Доставка
                                <img class="dost_image" src="images/deliv-image/dostavka.jpg" alt="dostavka" /></p></span>
                                        <span class="kagkopam_dost"></span>
                                    </legend>
                                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A6bb5917784994ba52e17c3468d9cbf0e3d1539399b1783c6769aa071e024b614&amp;width=850&amp;height=603&amp;lang=ru_RU&amp;scroll=true"></script>
                                    <p class="maps_text"><b>Мы доставляем курьером в</b> <a href="https://yandex.ru/maps/2/saint-petersburg/?ll=30.443132%2C59.758912&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.379522%252C59.807224%26spn%3D0.266733%252C0.140747%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%25A1%25D0%25B0%25D0%25BD%25D0%25BA%25D1%2582-%25D0%259F%25D0%25B5%25D1%2582%25D0%25B5%25D1%2580%25D0%25B1%25D1%2583%25D1%2580%25D0%25B3%252C%2520%25D0%259F%25D1%2583%25D1%2588%25D0%25BA%25D0%25B8%25D0%25BD%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B9%2520%25D1%2580%25D0%25B0%25D0%25B9%25D0%25BE%25D0%25BD%252C%2520%25D0%25BF%25D0%25BE%25D1%2581%25D1%2591%25D0%25BB%25D0%25BE%25D0%25BA%2520%25D0%25A8%25D1%2583%25D1%2588%25D0%25B0%25D1%2580%25D1%258B%2520&source=serp_navig&z=11">Шушары</a>,
                                        <a href="https://yandex.ru/maps/?ll=30.446618%2C60.054109&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.448408%252C60.045654%26spn%3D0.056656%252C0.038155%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259B%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25BD%25D0%25B3%25D1%2580%25D0%25B0%25D0%25B4%25D1%2581%25D0%25BA%25D0%25B0%25D1%258F%2520%25D0%25BE%25D0%25B1%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258C%252C%2520%25D0%2592%25D1%2581%25D0%25B5%25D0%25B2%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B6%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B9%2520%25D1%2580%25D0%25B0%25D0%25B9%25D0%25BE%25D0%25BD%252C%2520%25D0%25BF%25D0%25BE%25D1%2581%25D1%2591%25D0%25BB%25D0%25BE%25D0%25BA%2520%25D0%259C%25D1%2583%25D1%2580%25D0%25B8%25D0%25BD%25D0%25BE%2520&source=serp_navig&z=13">Мурино</a>,
                                        <a href="https://yandex.ru/maps/2/saint-petersburg/?ll=30.250363%2C60.086838&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.264393%252C60.080196%26spn%3D0.233986%252C0.077744%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%25A1%25D0%25B0%25D0%25BD%25D0%25BA%25D1%2582-%25D0%259F%25D0%25B5%25D1%2582%25D0%25B5%25D1%2580%25D0%25B1%25D1%2583%25D1%2580%25D0%25B3%252C%2520%25D0%25BF%25D0%25BE%25D1%2581%25D1%2591%25D0%25BB%25D0%25BE%25D0%25BA%2520%25D0%259F%25D0%25B0%25D1%2580%25D0%25B3%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B2%25D0%25BE%2520&source=serp_navig&z=12">Парголово</a>,
                                        <a href="https://yandex.ru/maps/?ll=30.401720%2C60.067636&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.399874%252C60.069954%26spn%3D0.044950%252C0.035824%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259B%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25BD%25D0%25B3%25D1%2580%25D0%25B0%25D0%25B4%25D1%2581%25D0%25BA%25D0%25B0%25D1%258F%2520%25D0%25BE%25D0%25B1%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258C%252C%2520%25D0%2592%25D1%2581%25D0%25B5%25D0%25B2%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B6%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B9%2520%25D1%2580%25D0%25B0%25D0%25B9%25D0%25BE%25D0%25BD%252C%2520%25D0%25BF%25D0%25BE%25D1%2581%25D1%2591%25D0%25BB%25D0%25BE%25D0%25BA%2520%25D0%2591%25D1%2583%25D0%25B3%25D1%2580%25D1%258B%2520&source=serp_navig&z=13">Бугры</a> ,
                                        <a href="https://yandex.ru/maps/?ll=30.476828%2C60.055565&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.476372%252C60.057323%26spn%3D0.025244%252C0.017578%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259B%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25BD%25D0%25B3%25D1%2580%25D0%25B0%25D0%25B4%25D1%2581%25D0%25BA%25D0%25B0%25D1%258F%2520%25D0%25BE%25D0%25B1%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258C%252C%2520%25D0%2592%25D1%2581%25D0%25B5%25D0%25B2%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B6%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B9%2520%25D1%2580%25D0%25B0%25D0%25B9%25D0%25BE%25D0%25BD%252C%2520%25D0%25B4%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BD%25D1%258F%2520%25D0%259D%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B5%2520%25D0%2594%25D0%25B5%25D0%25B2%25D1%258F%25D1%2582%25D0%25BA%25D0%25B8%25D0%25BD%25D0%25BE%2520&source=serp_navig&z=14">Новое Девяткино</a>,
                                        <a href="https://yandex.ru/maps/?ll=30.511252%2C59.903814&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.513577%252C59.908489%26spn%3D0.030806%252C0.035450%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259B%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25BD%25D0%25B3%25D1%2580%25D0%25B0%25D0%25B4%25D1%2581%25D0%25BA%25D0%25B0%25D1%258F%2520%25D0%25BE%25D0%25B1%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258C%252C%2520%25D0%2592%25D1%2581%25D0%25B5%25D0%25B2%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B6%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B9%2520%25D1%2580%25D0%25B0%25D0%25B9%25D0%25BE%25D0%25BD%252C%2520%25D0%2597%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B2%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B5%2520%25D0%25B3%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B5%2520%25D0%25BF%25D0%25BE%25D1%2581%25D0%25B5%25D0%25BB%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25B5%252C%2520%25D0%259A%25D1%2583%25D0%25B4%25D1%2580%25D0%25BE%25D0%25B2%25D0%25BE%2520&source=serp_navig&z=13">Кудрово</a>.</p>
                                </fieldset>

                                <fieldset class="dleverytaingun">
                                    <legend class="mealitykadsets">
                              <span class="stryconnectivi"><p class="title_del">Оплата
                                <img class="oplata" src="images/deliv-image/dost.png" alt="dostavka" /></p></span>
                                        <span class="kagkopam"></span>
                                    </legend>
                                    <p class="dost_text">Оплата производится наличными или по банковской карте при получении заказа.</p>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<?php bottom();
footer();?>