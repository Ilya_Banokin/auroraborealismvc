<?php
require_once 'module/contact.php';
require_once 'mail.php';
top(); ?>


    <!-- content -->
    <section id="content">
    <div class="bg-top">
        <div class="bg-top-2">
            <div class="bg">
                <div class="bg-top-shadow">
                    <div class="main">
                        <div class="box">
                            <div class="padding">
                                <div class="container_12">
                                    <div class="wrapper">
                                        <div class="grid_12">
                                            <div class="indent-left">
                                                <h3 class="p2">Контакты</h3>
                                                <div class="logoS">
                                                    <a href="#"> <img src="/images/logo.png"
                                                                      alt="Магазин рыбы и морепродуктов || Логотип Северное Сияние"></a>
                                                </div>
                                                <div class="contacts">
                                                    <h4>"Северное Сияние"</h4>
                                                    <p>г. Санкт-Петербург, Московское шоссе, 167</p>
                                                    <p>Телефон: +7(812)-309-95-25 , +7(921)-320-49-64 (Алексей)</p>
                                                    <p>Email: info@aurora-borealis.ru</p>
                                                </div>
                                                <div class="timeWork">
                                                    <h4>График работы:</h4>
                                                    <p>Понедельник - пятница с 10:00 до 22:00</p>
                                                    <p>Суббота, Воскресенье - Круглосуточно </p>
                                                </div>
                                                <div class="formsS"><a class="popup-open" id="button6" href="#">Обратная
                                                        связь</a></div>
                                                <div class="popup-fade">
                                                    <div class="popup">
                                                        <a class="popup-close" href="#">Закрыть</a>
                                                        <h3 class="p2">Форма обратной связи</h3>
                                                        <form id="contact-form" enctype="multipart/form-data">
                                                            <fieldset>
                                                                <label><span class="text-form">Ваше имя:</span><input
                                                                            id="name" type="text"/></label>
                                                                <label><span class="text-form">Email:</span><input
                                                                            id="email" type="text"
                                                                            value="<?= $_SESSION['email'] ?>"/></label>
                                                                <label><span class="text-form">Телефон:</span><input
                                                                            id="phone" type="text"/></label>
                                                                <div class="wrapper">
                                                                    <div class="text-form">Cообщение:</div>
                                                                    <textarea id="message"></textarea></div>
                                                                <div class="buttons">
                                                                    <a class="button"
                                                                       onclick="post_query('mail','contact','name.email.phone.message')">Отправить</a>
                                                                </div>
                                                            </fieldset>
                                                        </form>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function ($) {
            $('.popup-open').click(function () {
                $('.popup-fade').fadeIn();
                return false;
            });

            $('.popup-close').click(function () {
                $(this).parents('.popup-fade').fadeOut();
                return false;
            });

            $(document).keydown(function (e) {
                if (e.keyCode === 27) {
                    e.stopPropagation();
                    $('.popup-fade').fadeOut();
                }
            });

            $('.popup-fade').click(function (e) {
                if ($(e.target).closest('.popup').length == 0) {
                    $(this).fadeOut();
                }
            });
        });
    </script>

<?php bottom();
footer(); ?>