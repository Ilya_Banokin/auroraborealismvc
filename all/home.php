<?php top();
require_once 'module/home.php';
$slider = slider();
include 'module/menu.php';
?>

    <section id="content">
        <div class="bg-top">
            <div class="bg-top-2">
                <div class="bg">
                    <div class="bg-top-shadow">
                        <div class="main">
                            <div class="box">
                                <div class="padding">
                                    <div class="container_12">
                                        <div class="wrapper">
                                            <div class="grid_12">
                                                <div class="inner">
                                                    <div class="wrapper">
                                                        <div class="extra-wrap indent-top2">
                                                            <p style="text-align: center;"><img src="images/dost.jpg"
                                                                                                alt="Организация питания в офисы и на предприятия Санкт-Петербурга"/>
                                                            </p>
                                                            <h3 style="text-align: center;margin-top: 15px;">Организация питания в
                                                                офисы и на предприятия Санкт-Петербурга</h3>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="wrapper p4">
                                                    <?php
                                                    $categories = get_categories();
                                                    ?>
                                                    <?php foreach ($categories as $cat): ?>
                                                        <article class="grid_4 alpha">
                                                            <div class="indent-left">
                                                                <figure class="frame2 p2"><a
                                                                            href="<?= $cat['link']; ?>"><img
                                                                                src="images/cat-tovar/<?= $cat['img_mini']; ?>"
                                                                                alt="<?= $cat['alt']; ?>"/></a>
                                                                </figure>
                                                                <p class="cat_tovar"><a
                                                                            href="<?= $cat['link']; ?>"><?= $cat['title']; ?></a>
                                                                </p>
                                                                <p class="descriptions">
                                                                    <?= mb_substr($cat['description'], 0, 68, 'UTF-8') . '....'; ?>
                                                                </p>
                                                                <div class="wrapper"><br/>
                                                                    <a class="button2 fright"
                                                                       href="<?= $cat['link']; ?>">Просмотр
                                                                        категории</a>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->
    <div class="bg-top">
        <div class="bg-top-2">
            <div class="bg">
                <div class="bg-top-shadow">
                    <div class="main">
                        <div class="gallery">
                            <div class="wrapper indent-bot">
                                <div id="gallery" class="content">
                                    <div class="wrapper">
                                        <div class="slideshow-container">
                                            <div id="slideshow" class="slideshow"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="thumbs" class="navigation">
                                    <ul class="thumbs noscript">
                                        <?php foreach ($slider as $s): ?>
                                            <li>
                                                <a class="thumb" href="images/slide/<?= $s['image'] ?>"
                                                   title="Магазин рыбы и морепродуктов в Санкт Петербурге Северное Сияние">
                                                    <img src="images/slide/slider_mini/<?= $s['mini_image'] ?>"
                                                         alt="Магазин рыбы и морепродуктов в Санкт Петербурге Северное Сияние"/><span></span>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="inner">
                                <div class="wrapper">
                                    <span class="title img-indent3">Welcome!</span>
                                    <div class="extra-wrap indent-top2">
                                        В нашем <strong>рыбном магазине </strong>широкий ассортимент отличной рыбы,
                                        икры, морепродуктов, вяленой, копчёной рыбы, филе, стейков и деликатесов. <a
                                                target="_blank" href="https://www.aurora-borealis.ru">«СЕВЕРНОЕ
                                            СИЯНИЕ»</a> - отличный интернет-магазин, с помощью которого Вы можете
                                        заказать рыбу морепродукты практически в любой район Санкт-Петербурга. Вы
                                        спрашиваете, где купить рыбу? Отвечаем: <a target="_blank"
                                                                                   href="https://yandex.ru/maps/2/saint-petersburg/?from=api-maps&l=map&ll=30.442249%2C59.779229&mode=usermaps&origin=jsapi_2_1_72&um=constructor%3A8b5cf682707c180b6868635c2947eebac493b553725a5ab8fbc410b6a02c4979&z=17">Московское
                                            шоссе,167</a>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>
    </div>
    </div>

<?php bottom();
footer(); ?>