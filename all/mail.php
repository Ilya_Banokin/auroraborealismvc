<?php
require_once 'module/contact.php';
if ($_POST['contact_f'])
{
    name_valid ();
    email_valid ();
    phone_valid ();
    message_valid ();

    $subject = 'Обращение с сайта';
    $message = "<b>Имя:</b> $_POST[name];<br /><b>E-mail:</b> $_POST[email];<br /><b>Телефон:</b> ".htmlspecialchars($_POST['phone']).";<br /><b>Сообщение:</b> ".htmlspecialchars($_POST['message']).";";
    $headers = "Content-Type: text/html; charset=UTF-8";
    mail('info@aurora-borealis.ru',$subject,$message,$headers);

    message('Сообщение отправлено');
}