<?php
include 'guestForm.php';
if(!$_SESSION['confirm']['code'])
{
    not_found();
}
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Подтверждение</title>
    <link rel="stylesheet" href="/css/login/css/reset.css">
    <link rel="stylesheet" href="/css/login/css/animate.css">
    <link rel="stylesheet" href="/css/login/css/styles.css">
    <script src="/js/script.js" type="text/javascript"></script>
    <script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
</head>
<body>
<div id="container">
    <label for="name">Код:</label>
    <input type="name" id="code">

    <div id="lower">
        <input type="submit" onclick="post_query('guestForm','confirm','code')" value="Подтвердить">
    </div>

</div>
</body>
</html>