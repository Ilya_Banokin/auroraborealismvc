<?php
include 'module/pricing.php';
top();
$post_id = get_id_price ();
?>
<?php foreach ($post_id as $post):?>
    <!-- content -->
    <section id="content">
    <div class="bg-top">
        <div class="bg-top-2">
            <div class="bg">
                <div class="bg-top-shadow">
                    <div class="main">
                        <div class="box">
                            <div class="padding">
                                <div class="wrapper">
                                    <div class="name_tovar">
                                        <?=$post['name']; ?>
                                        <?php
                                        ## СЧЕТЧИК ПРОСМОТРОВ
                                        $db = new Db();
                                        $params = [
                                            'view' => $post['view'] + 1,
                                            'id' => intval($_GET['id']),
                                        ];
                                        $up_view = $db->query("UPDATE `pricing` SET `view` = :view WHERE `id` = :id",$params);
                                        ?>
                                        <div class="views">
                                            Просмотров:<?=$post['view']; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="image_tovar">
                                        <img src="../images/pricing/<?=$post['image']; ?>" alt="<?=$post['alt']; ?>" />
                                    </div>

                                    <div class="shop">
                                        <a href="#" class="buy"><?=$post['price']; ?> &#8381</a>
                                    </div>
                                    <div class="tex">
                                    <h3>Состав:</h3>
                                    <?=$post['text']; ?>
                                    </div>
                                </div>
                                <br />
                                <div class="paginate back">
                                    <a href="/pricing">НАЗАД</a>
                                </div>

                                <script type="text/javascript">
                                    VK.init({apiId: 7112800, onlyWidgets: true});
                                </script>

                                <div id="vk_comments" style="margin-left: 100px;margin-top: 45px;"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Comments("vk_comments", {limit: 10, width: "800", attach: "*"});
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php
bottomMenu();
footer();
?>