<?php
require_once 'module/users/functionLog.php';
if ($_POST['login_f']) {
    captcha_valid();
    email_valid ();
    password_valid ();
    login_isset();

}elseif ($_POST['register_f']) {

    captcha_valid();
    email_valid ();
    password_valid ();
    email_isset ();


    $code = random_str(5);

    $_SESSION['confirm'] = array(
        'type' => 'register',
        'email' => $_POST['email'],
        'password' => $_POST['password'],
        'code' =>  $code,
    );

    $subject = 'Регистрация Северное Сияние';
    $message = "Код подтверждения регистрации с сайта:<b>$code</b>";
    $headers = "Content-Type: text/html; charset=UTF-8";
    mail($_POST['email'],$subject,$message,$headers);

    redirect('confirm');

}elseif ($_POST['recovery_f']) {

    captcha_valid();
    email_valid ();
    email_isset_recovery();

    $code = random_str(5);

    $_SESSION['confirm'] = array(
        'type' => 'recovery',
        'email' => $_POST['email'],
        'code' =>  $code,
    );


    $subject = 'Восстановление пароля Северное Сияние';
    $message = "Код подтверждения восстановление пароля:<b>$code</b>";
    mail($_POST['email'],$subject,$message);

    redirect('confirm');


}elseif ($_POST['confirm_f']) {

    if ($_SESSION['confirm']['type'] == 'register') {

        if ($_SESSION['confirm']['code'] != $_POST['code']) {
            message('Код подтверждения регистрации указан неверно');
        } else {
            users_add();
            unset($_SESSION['confirm']);
            redirect('login');
        }
    } else if ($_SESSION['confirm']['type'] == 'recovery') {
        if ($_SESSION['confirm']['code'] != $_POST['code']) {
            message('Код подтверждения регистрации указан неверно');
        } else {
            users_recovery();
            unset($_SESSION['confirm']);

            message('Ваш новый пароль: ' . users_recovery());
        }
    }else if ($_SESSION['confirm']['type'] == 'login'){

        if ($_SESSION['confirm']['code'] != $_POST['code'])
        {
            message('Код подтверждения регистрации указан неверно');
        }
        go_auth($_SESSION['confirm']['data']);

    }
}