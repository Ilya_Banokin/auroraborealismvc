<?php
include 'guestForm.php';
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Регистрация</title>
    <link rel="stylesheet" href="/css/login/css/reset.css">
    <link rel="stylesheet" href="/css/login/css/animate.css">
    <link rel="stylesheet" href="/css/login/css/styles.css">
    <script src="/js/script.js" type="text/javascript"></script>
    <script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
</head>
<body>
<div id="container">
    <label for="name">E-mail:</label>
    <input type="name" id="email">

    <label for="username">Пароль:</label>
    <p><a href="/login">Есть логин?</a></p>
    <input type="password" id="password">

    <label for="name">Капча</label>
    <input type="name" id="captcha" placeholder="<?=captcha_show()?>">

    <div id="lower">
        <input type="submit" onclick="post_query('guestForm','register','email.password.captcha')" value="Регистрация">
    </div>

</div>
</body>
</html>