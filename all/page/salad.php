<?php
include 'module/menu/salad.php';
$catSalad = categories_salad ();
foreach ($catSalad as $getTM);
top();
?>

    <!-- content -->
    <section id="content">
    <div class="bg-top">
    <div class="bg-top-2">
    <div class="bg">
    <div class="bg-top-shadow">
    <div class="main">
    <div class="box">
    <div class="padding">
    <div class="container_12">
    <div class="wrapper">
    <div class="grid_12">
    <div class="indent-left p2">
        <h3 class="p0"><?=$getTM['title']; ?></h3>
        <div class="main_top_menu">
            <p><?=$getTM['description']; ?></p>
            <hr>
        </div>
    </div>
    <div class="wrapper p4">
<?php
$page = isset($_GET['page']) ? $_GET['page'] : 1;
if (!(int)$page) exit('Ошибка ID');
$limit = 12;
$offset = $limit * ($page - 1);
$postSalad = get_posts_salad($limit,$offset);
foreach($postSalad as $val): ?>
    <article class="grid_4 alpha">
        <div class="indent-left">
            <figure class="frame2 p2"><a href="/salad/post_salad?id=<?=$val['id']; ?>"><img src="../images/cat-tovar/salad/mini_salad/<?=$val['mini_img']; ?>" alt="<?=$val['alt']; ?>" /></a></figure>
            <h4><?=$val['title']; ?></h4>
            <?=mb_substr($val['description'], 0, 75, 'UTF-8'); ?>
            <div class="wrapper">
                <span class="price fleft"><?=$val['weight'];?> гр./<?=$val['price']; ?> &#8381</span>
                <a class="button fright" href="/salad/post_salad?id=<?=$val['id']; ?>">Далее</a>
            </div>
        </div>
    </article>
<?php endforeach; ?>
    </div>
        <!-- НАЧАЛО ПАГИНАЦИИ -->
        <div class="paginate wrapper">
            <a href="/menu">КАТЕГОРИИ</a>
            <?php
            $db = new Db();

            $result = $db->column("SELECT COUNT(*) FROM `salad`");

            $pagesCount = ceil($result / $limit);
            // ЕСЛИ СТРАНИЦА МЕНЬШЕ ЛИБО РАВНА 1, ТО ВЫВЕДИ ЗНАК PREV
            if ($page != 1) {
                $prev = $page - 1;
                echo "<a href=\"?page=$prev\">&lang;</a>";
            }
            // ВЫВОД ПАГИНАТОРА ЧЕРЕЗ ЦИКЛ
            for ($i = 1; $i <= $pagesCount; $i++) {
                if ($page == $i) {
                    $class = ' class="active"';
                }else {
                    $class = '';
                }
                echo "<a href=\"?page=$i\"$class>$i</a>";
            }
            // ЕСЛИ НА СТРАНИЦЕ ЗАПИСИЕЙ СКОЛЬКО В $limit, ТО ВЫВОДИ ЗНАК NEXT
            if ($page < $pagesCount) {
                $next = $page + 1;
                echo "<a href=\"?page=$next\">&rang;</a>";
            }
            ?>
        </div>
        <!-- КОНЕЦ ПАГИНАЦИИ -->
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
bottomMenu();
footer();
?>