<?php
include 'module/menu.php';

top()?>

    <!-- content -->
    <section id="content">
    <div class="bg-top">
        <div class="bg-top-2">
            <div class="bg">
                <div class="bg-top-shadow">
                    <div class="main">
                        <div class="box">
                            <div class="padding">
                                <div class="container_12">
                                    <div class="wrapper">
                                        <div class="grid_12">
                                            <div class="indent-left p2">
                                                <h3 class="p0">Меню Ресторана</h3>
                                            </div>
                                            <div class="wrapper p4">
                                                <?php
                                                $categories = get_categories();
                                                ?>
                                                <?php foreach($categories as $cat): ?>
                                                    <article class="grid_4 alpha">
                                                        <div class="indent-left">
                                                            <figure class="frame2 p2"><a href="<?=$cat['link']; ?>"><img src="images/cat-tovar/<?=$cat['img_mini']; ?>" alt="<?=$cat['alt']; ?>" /></a></figure>
                                                            <p class="cat_tovar"><a href="<?=$cat['link']; ?>"><?=$cat['title']; ?></a></p>
                                                            <p class="descriptions">
                                                                <?=mb_substr($cat['description'], 0, 60, 'UTF-8').'....'; ?>
                                                            </p>
                                                            <div class="wrapper"><br />
                                                                <a class="button2 fright" href="<?=$cat['link']; ?>">Просмотр категории</a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
bottomMenu();
footer();
?>

