<?php
function settings_restaurant_menu() {
    $db = new Db();

    $params = [
        'id' => 2,
    ];

    $titles = $db->row('SELECT * FROM `settings` WHERE `id` = :id',$params);

    return $titles;
}
function get_categories () {
    $db = new Db();

    $categories = $db->row('SELECT * FROM `categories`');

    return $categories;
}