<?php
function settings_delivery()
{
    $db = new Db();

    $params = [
        'id' => 4,
    ];

    $titles = $db->row('SELECT * FROM `settings` WHERE `id` = :id',$params);

    return $titles;
}