<?php
function message($text)
{
    exit ('{"message" :"' . $text . '"}');
}
function redirect($url)
{
    exit ('{"redirect" : "' . $url . '"}');
}
function random_str($num = 30)
{
    return substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),0,$num);
}
function captcha_show()
{
    $questions = array(

        1 => 'Столица России?',
        2 => 'В слове Ужин, пять букв?',
        3 => 'Напишите буквами число 3',
        4 => 'Столица Украины?',
        5 => 'Сколько будет 3 + 2?',
        6 => 'Вы человек?',
        7 => 'Название нашей планеты?',
    );

    $num = mt_rand(1,count($questions));
    $_SESSION['captcha'] = $num;

    echo $questions[$num];
}
function captcha_valid()
{
    $answers = array(

        1 => 'москва',
        2 => 'нет',
        3 => 'три',
        4 => 'киев',
        5 => 'пять',
        6 => 'да',
        7 => 'земля',
    );


    if ($_SESSION['captcha'] != array_search(mb_strtolower($_POST['captcha']),$answers))
    {
        message('Ответ на вопрос указан неверно');
    }

}
function email_valid ()
{
    if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
    {
        message('E-mail указан неверно');
    }
}
function password_valid ()
{
    if (!preg_match('/^[A-z0-9]{7,30}$/', $_POST['password']))

    {
        message('Пароль указан не верно и может содержать 7-30 символов A-z 0-9');
    }else{
        $_POST['password'] = md5($_POST['password']);
    }
}
function users_add()
{
    $db = new Db();

    $params = [
        'mailuser' => $_SESSION['confirm']['email'],
        'passuser' => $_SESSION['confirm']['password'],
        'ip' => '',
        'protected' => 0,
    ];

    if(is_numeric($_COOKIE['ref']))
    {
        $ref = $_COOKIE['ref'];
    }else{
        $ref = 0;
    }

    $db->query("INSERT INTO `users`(`email`,`password`,`ip`,`protected`) VALUES (:mailuser,:passuser,:ip,:protected)",$params);
}
function users_recovery()
{
    $db = new Db();

    $mailuser = $_SESSION['confirm']['email'];

    $newpass = random_str(10);

    $db->query("UPDATE `users` SET `password` = '" . md5($newpass) . "' WHERE `email` = '" . $mailuser . "'");

    return $newpass;
}
function email_isset ()
{
    $db = new Db();

    $params = [
        'email' => $_POST['email'],
    ];

    $emailIsset = $db->row('SELECT `id` FROM `users` WHERE `email` = :email',$params);

    if($emailIsset)
    {
        message("Этот E-mail уже занят или не существует");
    }
}
function email_isset_recovery()
{
    $db = new Db();

    $params = [
        'email' => $_POST['email'],
    ];

    if(!$db->row('SELECT `id` FROM `users` WHERE `email` = :email',$params))
    {
        message("Аккаунт не найден");
    }
}
function go_auth($data)
{
    foreach ($data as $key => $value)
    {
        $_SESSION[$key] = $value;
    }
    redirect('profile');
}
function login_isset()
{
    global $conn;

    $query = mysqli_query($conn,"SELECT * FROM `users` WHERE `email` = '$_POST[email]' and `password` = '$_POST[password]'");

    if(!mysqli_num_rows($query))
        message('Аккаунт не найден');


    $row = mysqli_fetch_assoc($query);

    if($row['ip'] )
    {
        $arr = explode(',', $row['ip']);

        if(!in_array($_SERVER['REMOTE_ADDR'],$arr))
        {
            message('Доступ запрещен для этого IP');
        }

    }

    if ($row['protected'] == 1)
    {
        $code = random_str(5);

        $_SESSION['confirm'] = array(
            'type' => 'login',
            'data' => $row,
            'code' =>  $code,
        );


        $subject = 'Подтверждение входа';
        $message = "Код подтверждения входа:<b>$code</b>";
        mail($_POST['email'],$subject,$message);

        redirect('confirm');
    }

    go_auth($row);

    redirect('profile');

}
function update_password()
{
    $db = new Db();

    $params = [
        'password' => $_POST['password'],
        'email' => $_SESSION['email'],
    ];

    $db->query( "UPDATE `users` SET `password` = :password WHERE `email` = :email",$params);

}
function add_ip()
{
    $db = new Db();

    $params = [
        'ip' => $_SESSION['ip'],
        'email' => $_SESSION['email'],
    ];

    $db->query( "UPDATE `users` SET `ip` = :ip WHERE `email` = :email",$params);

}
function protected_on_off()
{
    $db = new Db();

    $params = [
        'protected' => $_SESSION['protected'],
        'id' => $_SESSION['id'],
    ];

    $db->query("UPDATE `users` SET `protected` = :protected WHERE `id` = :id",$params);
}