<?php
function slider()
{
    $db = new Db();

    $slider = $db->row('SELECT * FROM `slider`');

    return $slider;
}
function settings_home()
{
    $db = new Db();

    $params = [
        'id' => 1,
    ];

    $titles = $db->row('SELECT * FROM `settings` WHERE `id` = :id',$params);

    return $titles;
}