<?php
function categories_drink () {
    $db = new Db();

    $params = [
        'id' => 5,
    ];

    $catDrink = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catDrink;
}
function get_posts_drink ($limit,$offset) {
    $db = new Db();

    $postDrink = $db->row("SELECT * FROM `drink` ORDER BY `id` DESC LIMIT $limit OFFSET $offset");

    return $postDrink;
}
function get_id_drink ()
{
    $db = new Db();

    if(!(int)$_GET['id'])
    {
        exit('ERROR ID');
    }

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $post_id = $db->row('SELECT * FROM `drink` WHERE id = :id',$params);

    if(!$post_id)
    {
        exit('ERROR ID');
    }else{
        return $post_id;
    }
}