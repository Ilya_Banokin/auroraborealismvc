<?php
function categories_pizza () {
    $db = new Db();

    $params = [
        'id' => 7,
    ];

    $catPizza = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catPizza;
}
function get_posts_pizza($limit,$offset) {
    $db = new Db();

    $postPizza = $db->row("SELECT * FROM `pizza` ORDER BY `id` DESC LIMIT $limit OFFSET $offset");

    return $postPizza;
}
function get_id_pizza ()
{
    $db = new Db();

    if(!(int)$_GET['id'])
    {
        exit('ERROR ID');
    }

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $post_id = $db->row('SELECT * FROM `pizza` WHERE id = :id',$params);

    if(!$post_id)
    {
        exit('ERROR ID');
    }else{
        return $post_id;
    }
}