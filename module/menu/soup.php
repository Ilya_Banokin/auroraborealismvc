<?php
function categories_soup () {
    $db = new Db();

    $params = [
        'id' => 2,
    ];

    $catSoup = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catSoup;
}
function get_posts_soup($limit,$offset) {
    $db = new Db();

    $postSoup = $db->row("SELECT * FROM `soup` ORDER BY `id` DESC LIMIT $limit OFFSET $offset");

    return $postSoup;
}
function get_id_soup ()
{
    $db = new Db();

    if(!(int)$_GET['id'])
    {
        exit('ERROR ID');
    }

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $post_id = $db->row('SELECT * FROM `soup` WHERE id = :id',$params);

    if(!$post_id)
    {
        exit('ERROR ID');
    }else{
        return $post_id;
    }
}