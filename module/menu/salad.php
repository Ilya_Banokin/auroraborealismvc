<?php
function categories_salad () {
    $db = new Db();

    $params = [
        'id' => 1,
    ];

    $catSalad = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catSalad;
}
function get_posts_salad ($limit,$offset) {
    $db = new Db();

    $postSalad = $db->row("SELECT * FROM `salad` ORDER BY `id` DESC LIMIT $limit OFFSET $offset");

    return $postSalad;
}
function get_id_salad ()
{
    $db = new Db();

    if(!(int)$_GET['id'])
    {
        exit('ERROR ID');
    }

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $post_id = $db->row('SELECT * FROM `salad` WHERE id = :id',$params);

    if(!$post_id)
    {
        exit('ERROR ID');
    }else{
        return $post_id;
    }
}