<?php
function categories_garnish () {
    $db = new Db();

    $params = [
        'id' => 6,
    ];

    $catGarnish = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catGarnish;
}
function get_posts_garnish ($limit,$offset) {
    $db = new Db();

    $postGarnish = $db->row("SELECT * FROM `garnish` ORDER BY `id` DESC LIMIT $limit OFFSET $offset");

    return $postGarnish;
}
function get_id_garnish ()
{
    $db = new Db();

    if(!(int)$_GET['id'])
    {
        exit('ERROR ID');
    }

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $post_id = $db->row('SELECT * FROM `garnish` WHERE id = :id',$params);

    if(!$post_id)
    {
        exit('ERROR ID');
    }else{
        return $post_id;
    }
}