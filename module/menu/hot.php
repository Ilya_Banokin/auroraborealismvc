<?php
function categories_hot () {
    $db = new Db();

    $params = [
        'id' => 3,
    ];

    $catHot = $db->row('SELECT * FROM `categories` WHERE `id` = :id',$params);

    return $catHot;
}
function get_posts_hot($limit,$offset) {
    $db = new Db();

    $postHot = $db->row("SELECT * FROM `hot_eat` ORDER BY `id` DESC LIMIT $limit OFFSET $offset");

    return $postHot;
}
function get_id_hot ()
{
    $db = new Db();

    if(!(int)$_GET['id'])
    {
        exit('ERROR ID');
    }

    $params = [
        'id' => (int)$_GET['id'],
    ];

    $post_id = $db->row('SELECT * FROM `hot_eat` WHERE id = :id',$params);

    if(!$post_id)
    {
        exit('ERROR ID');
    }else{
        return $post_id;
    }
}