<?php
function settings_contact()
{
    $db = new Db();

    $params = [
        'id' => 5,
    ];

    $titles = $db->row('SELECT * FROM `settings` WHERE `id` = :id',$params);

    return $titles;
}
function message_valid ()
{
    if (strlen($_POST['message']) < 10 or strlen($_POST['message']) > 120)
    {
        message('Длина сообщения должна составлять 10 - 120 символов');
    }
}
function name_valid ()
{
    if (!preg_match('/^([а-яА-ЯЁёa-zA-Z_]+)$/u', $_POST['name']))
    {
        message('Имя должно составлять от 3 - 10 символов А-я 0-9');
    }
}
function phone_valid ()
{
    if (!preg_match('/^[0-9]{5,15}$/', $_POST['phone']))
    {
        message('Телефон должен составлять от 5 - 15 символов 0-9');
    }
}