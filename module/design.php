<?php
require_once 'module/home.php';
function top ()
{
    if ($_SERVER['REQUEST_URI'] == '/')
    {
        $titles = settings_home();
        foreach ($titles as $val)
        {
            $title = $val['title'];
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu'){
        $titles = settings_restaurant_menu();
        foreach ($titles as $val)
        {
            $title = $val['title'];
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/pricing'){
        $titles = settings_pricing();
        foreach ($titles as $val)
        {
            $title = $val['title'];
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/delivery'){
        $titles = settings_delivery();
        foreach ($titles as $val)
        {
            $title = $val['title'];
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/contact'){
        $titles = settings_contact();
        foreach ($titles as $val)
        {
            $title = $val['title'];
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu/salad' or $_SERVER['REQUEST_URI'] == '/menu/salad?page='.(int)$_GET['page'].''){
        $catSalad = categories_salad ();
        foreach ($catSalad as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu/soup' or $_SERVER['REQUEST_URI'] == '/menu/soup?page='.(int)$_GET['page'].''){
        $catSalad = categories_soup ();
        foreach ($catSalad as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu/hot' or $_SERVER['REQUEST_URI'] == '/menu/hot?page='.(int)$_GET['page'].''){
        $catHot = categories_hot ();
        foreach ($catHot as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu/pizza' or $_SERVER['REQUEST_URI'] == '/menu/pizza?page='.(int)$_GET['page'].''){
        $catHot = categories_pizza ();
        foreach ($catHot as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }
    elseif($_SERVER['REQUEST_URI'] == '/menu/snack' or $_SERVER['REQUEST_URI'] == '/menu/snack?page='.(int)$_GET['page'].''){
        $catSnack = categories_snack ();
        foreach ($catSnack as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu/drink' or $_SERVER['REQUEST_URI'] == '/menu/drink?page='.(int)$_GET['page'].''){
        $catDrink = categories_drink ();
        foreach ($catDrink as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/menu/garnish' or $_SERVER['REQUEST_URI'] == '/menu/garnish?page='.(int)$_GET['page'].''){
        $catGarnish = categories_garnish ();
        foreach ($catGarnish as $val)
        {
            $title = $val['title'] .' || Меню ресторана кафе Северное Сияние в Санкт-Петербурге ';
            $meta_d = $val['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/salad/post_salad?id='.(int)$_GET['id'].''){
        $post_id = get_id_salad ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние ';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/soup/post_soup?id='.(int)$_GET['id'].''){
        $post_id = get_id_soup ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние ';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/hot/post_hot?id='.(int)$_GET['id'].''){
        $post_id = get_id_hot ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/pizza/post_pizza?id='.(int)$_GET['id'].''){
        $post_id = get_id_pizza ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние';
            $meta_d = $post['meta_d'];
        }
    }
    elseif($_SERVER['REQUEST_URI'] == '/snack/post_snack?id='.(int)$_GET['id'].''){
        $post_id = get_id_snack ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/drink/post_drink?id='.(int)$_GET['id'].''){
        $post_id = get_id_drink ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/garnish/post_garnish?id='.(int)$_GET['id'].''){
        $post_id = get_id_garnish ();

        foreach ($post_id as $post)
        {
            $title = $post['title'] .' || Магазин рыбы и морепродуктов в СПБ || Меню Ресторана кафе Северное Сияние';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/pricing/post_pricing?id='.(int)$_GET['id'].''){
        $post_id = get_id_price ();

        foreach ($post_id as $post)
        {
            $title = $post['name'] .' || Интернет магазин рыбы и морепродуктов в СПБ || Заказать по Санкт Петербургу || Бесплатная доставка Северное Сияние';
            $meta_d = $post['meta_d'];
        }
    }elseif($_SERVER['REQUEST_URI'] == '/profile')
    {
        $title = 'Профиль || Магазин рыбы и морепродуктов в СПБ || Ресторан кафе Северное Сияние || Интернет магазин в Санкт Петербурге';
    }elseif($_SERVER['REQUEST_URI'] == '/profile/edit')
    {
        $title = 'Редакировать профиль || Магазин рыбы и морепродуктов в СПБ || Ресторан кафе Северное Сияние || Интернет магазин в Санкт Петербурге';
    }

    echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/style_button.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/grid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/header.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/tovar.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/delivery.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/pagination.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/tbprice.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/price.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/profile.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/formContact.css" type="text/css" media="screen">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Philosopher&display=swap" rel="stylesheet">
    <script id="dsq-count-scr" src="/aurora-borealis-ru.disqus.com/count.js" async></script>
    <script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="/js/jquery.galleriffic.js" type="text/javascript"></script>
    <script src="/js/jquery.opacityrollover.js" type="text/javascript"></script>
    <script src="/js/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="/js/jcarousellite_1.0.1.js" type="text/javascript"></script>
    <script src="/js/script.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?162"></script>
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">     
    <!--[if lt IE 7]>
        <div style=\' clear: both; text-align:center; position: relative;\'>
            <a href="https://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="https://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"  alt="" /></a>
        </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5.js"></script>
        <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
    <![endif]-->
    <meta name="description" content="'.$meta_d.'">
    <title>'.$title.'</title>
</head>
<body id="page1">
	<!--==============================header=================================-->
    <header>
    <div class="row-1">
        	<div class="main">
                   	<div class="container_12">
                	<div class="grid_12">
                    	<nav>
                            <ul class="menu">
                                <li><a id="one" href="/">Главная</a></li>
                                <li><a id="two" href="/menu">Меню ресторана</a></li>
                                <li><a id="three" href="/pricing">Прайс Лист</a></li>
                                <li><a id="four" href="/delivery">Доставка</a></li>
                                <li><a id="five" href="/contact">Контакты</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>';
    if($_SESSION['id'])
    {
        echo '
        <div class="row-2">

        	<div class="main">
            	<div class="container_12">
                	<div class="grid_9">
                    	<h1>
                            <a class="logo" href="index.html">Север<strong>н</strong>ое</a>
                            <span>Сияние</span>
                        </h1>
                    </div>
                    <div class="profile">
                    <img class="profileAvatar" src="/images/user_defult.png" width="40px" height="40px" alt="ProfileAuth">
                        <p style="font-size: 18px"><b>Здравствуйте,</b></p>
                        <p style="margin-top: 5px">Вы вошли как:</p>
                        <p><b style="color: #368DB2;font-size: 16px;">'.$_SESSION['email'].'</b></p>
                    </div>
                    <div class="container-contact">
                        <p>Тел: +7(812)-309-95-25 <br />
                        <a href="mailto:info@aurora-borealis.ru">info@aurora-borealis.ru</a></p>
                    </div>
                </div>
            </div>
        </div>
        
        ';
    }else {

        echo '
                <div class="row-2">
        
        	<div class="main">
            	<div class="container_12">
                	<div class="grid_9">
                    	<h1>
                            <a class="logo" href="index.html">Север<strong>н</strong>ое</a>
                            <span>Сияние</span>
                        </h1>
                    </div>
                    
                    <div class="container-zakaz">
                       <form action="/submit.php" method="POST">
                        <p>Ваше имя:<br />
                           <input class="input-header" type="name" name="name" placeholder="Введите имя" /><br />
                           Телефон:<br />
                           <input class="input-header" name="phone" placeholder="+7 (***) ** **" /></p>
                           <input type="submit" class="button4" value="Заказать звонок">
                       </form> 
                    </div>
                 
                    <div class="container-contact">
                        <p>Тел: +7(812)-309-95-25 <br />
                        <a href="mailto:info@aurora-borealis.ru">info@aurora-borealis.ru</a></p>
                    </div>
                     <img src="../images/dostavka_na_dom.png" style="width: 402px; height: 129px; position: relative;float: right; bottom: 51px; right: 10px;"">
                </div>
            </div>
        </div>
        
        ';
    }
    if(!$_SESSION['id']) {
        echo '<a href="/login"><img class="log" align="right" src="/images/login.png"></a></header>';
    }else{
        echo '<a href="/logout"><img class="logout" align="left" src="/images/logout.png"></a></header>';
}
    echo '</header>';
}
function bottom ()
{
    require_once 'module/news.php';
    $news = news ();
    echo '
    <div class="bg-bot">
        	<div class="main">
            	<div class="container_12">
                	<div class="wrapper">
                    	<article class="grid_4">
                        	<h3 class="prev-indent-bot">Контакты</h3>
                            <p class="prev-indent-bot"><p>
                            Телефон: +7 (812) 309-95-25<br>
                            E-mail: <a href="mailto:zakaz@aurora-borealis.ru">zakaz@aurora-borealis.ru</a><br>
                            Адрес: <a target="_blank" href="https://yandex.ru/maps/2/saint-petersburg/?from=api-maps&l=map&ll=30.442249%2C59.779229&mode=usermaps&origin=jsapi_2_1_72&um=constructor%3A8b5cf682707c180b6868635c2947eebac493b553725a5ab8fbc410b6a02c4979&z=17">Московское шоссе,167</a></p>
                                
                        </article>
                        <article class="grid_4">
                        	<h3 class="prev-indent-bot">Отзывы</h3>
                            <div class="quote">
                            	<p class="prev-indent-bot">Прекрасный магазин, быстрая доставка. Очень вежливые продавцы. Советуем!</p>
                                <h5>Открытый Кабельный Портал</h5>
                                Менеджер
                            </div>
                        </article>
                        <article class="grid_4">
                        	<h3 class="prev-indent-bot">Что Нового?</h3>';
                        	foreach ($news as $n):
                           echo ' <time class="tdate-1"><a class="link" href="#">'.$n['date'].'</a></time>'.$n['text'];
                        	endforeach;
                    echo ' </article>
                    </div>
                </div>
            </div>
        </div>
    </section>';
}
function bottomMenu()
{
    require_once 'module/pricing.php';
    $random = random_price_left ();
    $randomRight = random_price_right ();
    $lastPrice = last_price ();
    foreach ($lastPrice as $lp):
    echo '
    <div class="bg-bot">
            <div class="main">
                <div class="container_12">
                    <div class="wrapper">
                        <article class="grid_4">
                            <h3 class="p2">Новый Товар</h3>
                            <div class="wrapper">
                                <figure class="img-indent frame2"><a href="/pricing/post_pricing?id='.$lp['id'].'"><img src="/images/page4-img7.jpg" alt="Новый Товар в Интернет магазине Северное Сияние" /></a></figure>
                                <p class="prev-indent-bot color-4">
                                    <a href="/pricing/post_pricing?id='.$lp['id'].'">'.$lp['name'].'</a></p>
                            </div>
                        </article>';
    endforeach;
    echo ' <article class="grid_8">
                            <h3 class="prev-indent-bot2">Прайс-Лист</h3>
                            <div class="wrapper">
                                <div class="grid_4 alpha">
                                    <div class="indent-right2">
                                        <ul class="price-list"> ';
    foreach ($random as $r):
        echo '
                                            <li><span>  
                                                    &#8381;  '.$r['price'].'</span><a href="/pricing/post_pricing?id='.$r['id'].'">
                                                '.mb_substr($r['name'],0,32,'UTF-8').'
                                                </a><strong>&nbsp;</strong></li>';
    endforeach;
    echo '
                                        </ul>
                                    </div>
                                </div>
                            <div class="grid_4 omega">
                                    <div class="indent-right2">
                                        <ul class="price-list">';
                                foreach ($randomRight as $rr):
                                    echo '
                                            <li><span>  
                                                    &#8381; '.$rr['price'].'</span><a href="/pricing/post_pricing?id='.$rr['id'].'">
                                                '.mb_substr($rr['name'],0,32,'UTF-8').'
                                                </a><strong>&nbsp;</strong></li>';
                                endforeach;
                                echo '
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    ';
}
function footer()
{
    echo '
    <footer>
    <div class="main">
        <div class="container_12">
            <div class="wrapper">
                <div class="grid_4">
                    <p>Северное Сияние &copy; 2020
<u><a href="#" target="_blank">Наши партнеры</a></u>&nbsp;|<br> ИП Радько А.Л.</p>
                        <!-- {%FOOTER_LINK} -->
                </div>
                
                    <div class="grid_4">
                        <span class="phone-numb"><span>+7(812)</span> 309-95-25</span>
                    </div>

                    <div class="grid_4">
                    	<ul class="list-services">
                        	<li><a href="vk.com"></a></li>
                            <li><a class="item-2" href="facebook.ru"></a></li>
                            <li><a class="item-3" href="instagram.ru"></a></li>
                            <li><a class="item-4" href="tweeter.ru"></a></li>
                        </ul>
                    </div>
            </div>

        </div>
    </div>
</footer>
    <script src="/js/slider.js" type="text/javascript"></script>
    ';
}