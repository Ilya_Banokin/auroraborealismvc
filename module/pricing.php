<?php
function settings_pricing()
{
    $db = new Db();

    $params = [
        'id' => 3,
    ];

    $titles = $db->row('SELECT * FROM `settings` WHERE `id` = :id',$params);

    return $titles;
}
function get_posts_price ($limit,$offset)
{
    $db = new Db();

    $postPricing = $db->row("SELECT * FROM `pricing` LIMIT $limit OFFSET $offset");

    return $postPricing;
}
function get_id_price ()
{
    $db = new Db();

    $params = [
        'id' => intval($_GET['id']),
    ];

    if (!is_numeric($_GET['id'])) not_found();

    $post_id = $db->row('SELECT * FROM `pricing` WHERE id = :id',$params);

    return $post_id;
}
function last_price () {

    $db = new Db();

    $lastPrice = $db->row('SELECT * FROM `pricing` ORDER BY id DESC LIMIT 1');

    return $lastPrice;
}
function random_price_left () {

    $db = new Db();

    $random =  $db->row('SELECT * FROM `pricing` WHERE `id` < 30 ORDER BY RAND() LIMIT 5');

    return $random;
}
function random_price_right () {

    $db = new Db();

    $randomRight = $db->row('SELECT * FROM `pricing` WHERE `id` > 30 ORDER BY RAND() LIMIT 5');

    return $randomRight;
}