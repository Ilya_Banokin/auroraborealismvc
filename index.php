<?php
include 'module/design.php';
include 'module/config.php';

if ($_SERVER['REQUEST_URI'] == '/')
{
    $page = 'index';
    $module = 'index';
}else {
    $url_path = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
    $url_parts = explode('/',trim($url_path, ' /'));
    $page = array_shift($url_parts);
    $module = array_shift($url_parts);

    if (!preg_match('/^[A-z0-9]{3,15}$/',$page))
    {
        not_found();
    }


    if (!empty($module))
    {
        $Param = array();
        for ($i = 0; $i < count($url_parts); $i++)
        {
            $Param[$url_parts[$i]] = $url_parts[++$i];
        }
    }
}


session_start();
include 'Db.php';
include 'dbtest.php';
require_once 'all/guestForm.php';
require_once 'auth/authForm.php';
require_once 'all/mail.php';

if ($_SESSION['id'] and $page == 'profile'){
    if($_SESSION['id'] and $page == 'profile' and $module == 'edit') include 'auth/editProfile.php';
    else
    include 'auth/profile.php';
}
elseif (!$_SESSION['id'] and $page == '/' or $page == 'index') include 'all/home.php';
elseif ($_SESSION['id'] and $page == 'logout') include 'auth/logout.php';
elseif ($page == 'menu')
{
    if($page == 'menu' and $module == 'salad') include 'all/page/salad.php';
    elseif ($page == 'menu' and $module == 'soup') include 'all/page/soup.php';
    elseif ($page == 'menu' and $module == 'hot') include 'all/page/hot.php';
    elseif ($page == 'menu' and $module == 'pizza') include 'all/page/pizza.php';
    elseif ($page == 'menu' and $module == 'snack') include 'all/page/snack.php';
    elseif ($page == 'menu' and $module == 'drink') include 'all/page/drink.php';
    elseif ($page == 'menu' and $module == 'garnish') include 'all/page/garnish.php';
    elseif ($page == 'menu') include 'all/menu.php';
    else not_found();
}
elseif ($page == 'salad' and $module == 'post_salad') include 'all/post/post_salad.php';
elseif ($page == 'soup' and $module == 'post_soup') include 'all/post/post_soup.php';
elseif ($page == 'hot' and $module == 'post_hot') include 'all/post/post_hot.php';
elseif ($page == 'pizza' and $module == 'post_pizza') include 'all/post/post_pizza.php';
elseif ($page == 'snack' and $module == 'post_snack') include 'all/post/post_snack.php';
elseif ($page == 'drink' and $module == 'post_drink') include 'all/post/post_drink.php';
elseif ($page == 'garnish' and $module == 'post_garnish') include 'all/post/post_garnish.php';
elseif ($page == 'pricing' and $module == 'post_pricing') include 'all/post/post_pricing.php';
elseif ($page == 'pricing') include 'all/pricing.php';
elseif ($page == 'delivery') include 'all/delivery.php';
elseif ($page == 'contact') include 'all/contact.php';
elseif (!$_SESSION['id'] and $page == 'login') include 'all/login.php';
elseif (!$_SESSION['id'] and $page == 'register') include 'all/register.php';
elseif (!$_SESSION['id'] and $page == 'recovery') include 'all/recovery.php';
elseif (!$_SESSION['id'] and $page == 'confirm') include 'all/confirm.php';
elseif ($page == 'admin' ) {
    if($page == 'admin' and $module == 'amenu') include 'admin/menu/iMenu.php';
    elseif ($page == 'admin' and $module == 'asalad') include 'admin/menu/iSalad.php';
    elseif ($page == 'admin' and $module == 'asoup') include 'admin/menu/iSoup.php';
    elseif ($page == 'admin' and $module == 'ahot') include 'admin/menu/iHot.php';
    elseif ($page == 'admin' and $module == 'apizza') include 'admin/menu/iPizza.php';
    elseif ($page == 'admin' and $module == 'asnack') include 'admin/menu/iSnack.php';
    elseif ($page == 'admin' and $module == 'adrink') include 'admin/menu/iDrink.php';
    elseif ($page == 'admin' and $module == 'agarnish') include 'admin/menu/iGarnish.php';
    elseif ($page == 'admin' and $module == 'apricing') include 'admin/pricing/iPricing.php';
    elseif ($page == 'admin' and $module == 'anews') include 'admin/news/iNews.php';
    elseif ($page == 'admin' and $module == 'aslider') include 'admin/slider/iSlider.php';
    elseif($page == 'admin') include 'admin/index.php';
    else not_found();
}
else not_found();